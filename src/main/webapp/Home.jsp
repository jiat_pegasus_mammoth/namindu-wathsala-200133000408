<%@ page import="com.jiat.web.db.DBConnection" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.ResultSet" %>
<%--
  Created by IntelliJ IDEA.
  User: namindu
  Date: 3/31/23
  Time: 10:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        table {
            width: 90%;
            text-align: center;
        }

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th {
            background-color: #96D4D4;
        }

        .inp {
            border: none;
            outline: none;
            width: 100%;
            text-align: center;
        }

        .btndl {
            background-color: red;
        }

        .btnup {
            background-color: yellow;
        }

        .idwid {
            width: 70px;
        }
    </style>
</head>
<body>
<h1>Home</h1>

<%
    Connection connection = null;
    try {
        connection = DBConnection.getConnection();
        ResultSet rs = DBConnection.search("SELECT * FROM `web_db`.`user`");
        if (rs.next()) {

%>
    <table>
        <tr>
            <th class="idwid">Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Update</th>
            <th>Delete</th>
        </tr>

        <%
            while (rs.next()) {
        %>
        <form action="UpdateProcess" method="post">
        <tr>
            <td class="idwid"><input class="inp" type="text" readonly name="id" value="<%=rs.getString("id")%>">
            </td>
            <td><input class="inp" type="text" name="name" value="<%=rs.getString("name")%>">
            </td>
            <td><input class="inp" type="text" name="email" value="<%=rs.getString("email")%>">
            </td>
            <td><input class="inp" type="text" name="password" value="<%=rs.getString("password")%>">
            </td>
            <td>
                <button class="btnup" type="submit">Update</button>
            </td>
            <td>
                <button class="btndl" type="submit">Delete</button>
            </td>
        </tr>
        </form>

        <%
            }
        %>
    </table>

<%
        } else {
        }
    } catch (Exception e) {
        e.printStackTrace();
    } finally {
        try {
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
%>

</body>
</html>
