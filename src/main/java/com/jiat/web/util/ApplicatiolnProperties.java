package com.jiat.web.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicatiolnProperties {

    private static ApplicatiolnProperties applicatiolnProperties;
    private Properties properties;
    private ApplicatiolnProperties() {
        properties = new Properties();
        InputStream is = getClass().getClassLoader().getResourceAsStream("application.properties");
        try {
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ApplicatiolnProperties getInstance(){
        if(applicatiolnProperties == null){
            applicatiolnProperties = new ApplicatiolnProperties();
        }
            return applicatiolnProperties;
    }

    public String get(String key){
        return properties.getProperty(key);
    }
}
