package com.jiat.web.model;

import com.jiat.web.db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

@WebServlet(name = "UpdateProcess", value = "/UpdateProcess")
public class UpdateProcess extends HttpServlet {

    Connection connection = null;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        if (!req.getParameter("name").isEmpty() && !req.getParameter("email").isEmpty() && !req.getParameter("password").isEmpty()) {
            try {
                connection = DBConnection.getConnection();
                ResultSet rs = DBConnection.search("SELECT * FROM `web_db`.`user` WHERE `name`='" + req.getParameter("name") + "' AND `email`='" + req.getParameter("email") + "' AND `password`='" + req.getParameter("password") + "' ;");
                if (rs.next()) {
                    resp.getWriter().write("You did not change anything");
                    resp.sendRedirect("Home.jsp");
                } else {
                    DBConnection.iud("UPDATE `web_db`.`user` SET `name`='"+req.getParameter("name")+"',`email`='"+req.getParameter("email")+"',`password`='"+req.getParameter("password")+"' WHERE `id`='"+req.getParameter("id")+"';");
                    resp.sendRedirect("Home.jsp");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            resp.getWriter().write("Please enter fill the text field to Sign in");
        }
    }

}
