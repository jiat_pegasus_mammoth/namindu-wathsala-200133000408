package com.jiat.web.model;

import com.jiat.web.db.DBConnection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

@WebServlet(name = "SigninProsess", value = "/SigninProsess")
public class SigninProsess extends HttpServlet {

    Connection connection = null;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        if (!req.getParameter("email").isEmpty() && !req.getParameter("password").isEmpty()) {
            try {
                connection = DBConnection.getConnection();
                ResultSet rs = DBConnection.search("SELECT * FROM `web_db`.`user` WHERE `email`='" + req.getParameter("email") + "' AND `password`='" + req.getParameter("password") + "' ;");
                if (rs.next()) {
                    resp.sendRedirect("Home.jsp");
                } else {
                    resp.getWriter().write("Invalid Email or Password");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            resp.getWriter().write("Please enter fill the text field to Sign in");
        }
    }

}
